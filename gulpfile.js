"use strict";

var gulp = require('gulp');
var gutil = require('gulp-util');
var chalk = require('chalk');
var babel = require("gulp-babel");
var webpack = require("webpack");
var webpackConfig = require("./webpack.config.js");
var webpackSpecConfig = require("./webpack.testing.config.js");
var browserSync = require('browser-sync').create();
var runSequence = require('run-sequence');
var rimraf = require('rimraf');
var chokidar = require('chokidar');
var path = require('path');
var eslint = require('gulp-eslint');
var sass = require('gulp-sass');

var config = {
    'app': './app',
    'development': './development'
};
var success = chalk.blue;
var error = chalk.orange;

// Static server
gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./development"
        }
    });
});

gulp.task('webpack:build', ['js:lint'], function (callback) {
    webpack(webpackConfig, function (err, stats) {
        if (err) {
            gutil.log(error('Error', err));
        }
        else {
            browserSync.reload();
            gutil.log(success("Successfully copied JS files"));
        }
    });
});

gulp.task('webpackspec:build', ['js:lint'], function (callback) {
    webpack(webpackSpecConfig, function (err, stats) {
        if (err) {
            gutil.log(error('Error', err));
        }
        else {
            gutil.log(success("Successfully copied Spec files"));
        }
    });
});

gulp.task('js:lint', function () {
    lintTask(path.join(config.app, "/js/**/*.js"))
});

gulp.task('html:copy', function (callback) {
    gulp.src(config.app + "/index.html")
        .pipe(gulp.dest(config.development))
        .pipe(browserSync.stream())
        .on('end', function () {
            gutil.log(success('successfully copied index.html'));
        })
        .on('error', function (err) {
            gutil.log(error(err));
        });
});

gulp.task('sass:build', function () {
    gulp.src((config.app + "/styles/**/*.scss"))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest(config.development + '/styles'))
        .pipe(browserSync.stream());
});

gulp.task('watch', function () {
    watcher(['./app/*.html'], function () {
        gulp.start('development:prepare')
    })
});

gulp.task('development:prepare', function (cb) {
    runSequence(['webpack:build', 'html:copy', 'sass:build', 'webpackspec:build']);
});

gulp.task('development:clean', function (cb) {
    clean(config.development, cb);
});

gulp.task('default', function (cb) {
    runSequence('browser-sync', 'watch', 'development:clean', 'development:prepare', cb);
});

function lintTask(files) {
    gulp.src(files)
        .pipe(babel())
        .on('error', function (err) {
            gutil.log(error(err.message));
            this.emit('end');
        })
        .pipe(eslint())
        .pipe(eslint.format());
}

function watcher(fileOrDirectory, callBack) {
    return chokidar.watch(fileOrDirectory, {ignored: /[\/\\]\./, ignoreInitial: true})
        .on('add', function () {
            gutil.log(success("File(s):ADD"));
            callBack();
        })
        .on('change', function () {
            gutil.log(success("File(s):CHANGE"));
            callBack();
        })
        .on('unlink', function () {
            gutil.log(success("File(s):DELETE"));
            callBack();
        })
        .on('error', function (error) {
            gutil.error(error('Error happened', error));
        })
}

function clean(globFolder, cb) {
    rimraf(globFolder, cb);
}








