"use strict";

import React from 'react';
import {createRenderer} from 'react-addons-test-utils';
import expectJSX from 'expect-jsx';
import expect from 'expect';

import MyComponent from '../app/js/components/MyComponent';
expect.extend(expectJSX);

describe("module", function () {

    it("should run tests", function () {
        expect(1).toEqual(1);
    });

    it("should run react test", function () {
        let renderer = createRenderer();
        renderer.render(<MyComponent/>);
        let actualElement = renderer.getRenderOutput();
        let expectedElement = <div>JNAANA!!</div>;
        expect(actualElement).toEqualJSX(expectedElement);
    });

});