var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        spec: [path.join(__dirname, 'webpack.test.bootstrap.js')]
    },
    output: {
        path: "./development/spec/",
        filename: "spec.js"
    },
    resolve: {
        root: "./app"
    },
    resolveLoader: {
        modulesDirectories: [
            './node_modules', './app', './spec'
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            'React': 'react',
            'ReactDOM': 'react-dom'
        })
    ],
    module: {
        loaders: [
            {test: /\.js$/, loader: 'babel-loader'}
        ]
    },
    watch: true
};