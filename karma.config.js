var webpackConfig = require('./webpack.testing.config');
webpackConfig.devtool = 'inline-source-map';

module.exports = function (config) {
    config.set({
        browsers: ['Chrome'],
        // karma only needs to know about the test bundle
        files: [
            './development/spec/spec.js'
        ],
        frameworks: ['chai', 'mocha'],
        plugins: [
            'karma-chrome-launcher',
            'karma-chai',
            'karma-mocha',
            'karma-sourcemap-loader',
            'karma-webpack'
        ],
        // run the bundle through the webpack and sourcemap plugins
        singleRun: false,
        // webpack config object
        reporters: [ 'dots' ],
        webpack: webpackConfig,
        webpackServer: {
            noInfo: true
        },
        autoWatch: true
    });
};