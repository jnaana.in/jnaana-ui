var webpack = require('webpack');

module.exports = {
    entry: "./app/js/index.js",
    output: {
        path: "./development/js",
        filename: "app.js"
    },
    resolve: {
        root: "./app"
    },
    resolveLoader: {
        modulesDirectories: [
            './node_modules'
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            'React': 'react',
            'ReactDOM': 'react-dom'
        })
    ],
    module: {
        loaders: [
            {test: /\.js$/, loader: 'babel-loader'},
        ]
    },
    watch: true
};